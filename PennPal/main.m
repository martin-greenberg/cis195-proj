//
//  main.m
//  PennPal
//
//  Created by Martin Greenberg on 11/13/14.
//  Copyright (c) 2014 dsmg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
